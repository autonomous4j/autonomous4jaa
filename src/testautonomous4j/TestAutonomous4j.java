package testautonomous4j;

import autonomous4j.A4jUtils;
import autonomous4j.Mission;
import autonomous4j.Pose3D;

/**
 *
 * @author Jim Weaver & Mark Heckler
 */
public class TestAutonomous4j {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("creating mission");
        Mission mission = A4jUtils.createMission();

        System.out.println("--> creating mission steps");
        try {
            System.out.println("--> Placing takeoff command in queue.");
            mission.takeoff().doFor(4000);
            System.out.println("--> F1");
            mission.moveTo(new Pose3D(1,0,0,0)).doFor(1000);
            System.out.println("--> Placing hover command in queue.");
            mission.hover().doFor(1000);
            System.out.println("--> B1");
            mission.moveTo(new Pose3D(-1,0,0,0)).doFor(500);
//            System.out.println("Placing moveto command in queue.");
//            mission.moveTo(new Pose3D(0, 0, 1, 0));
            System.out.println("--> Placing land command in queue.");
            mission.land();
            
            System.out.println("--> calling executeMission()");
            mission.executeMission();
        } catch (Exception e) {
            System.out.println("--> Exception running mission: " + e.getMessage());
//        } finally {
            mission.getClient().land();
            mission.getClient().stop();
        }
    }
}
