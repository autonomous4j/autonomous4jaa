/*
 * Camera.java
 */

package autonomous4j;

import java.awt.geom.Point2D;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

/**
 *
 * @author james_000
 */
public class Camera {
  
  // AR Drone 2.0 Bottom Camera Intrinsic Matrix
  // https://github.com/tum-vision/ardrone_autonomy/blob/master/calibrations/ardrone2_bottom/cal.ymli
  private static final RealMatrix K_BOTTOM = 
    MatrixUtils.createRealMatrix(new double[][]
      {{686.994766, 0, 329.323208},
       {0, 688.195055, 159.323007},
       {0, 0, 1}});

  private RealMatrix _k;
  private RealMatrix _invK;
  
  public Camera() {
    _k = K_BOTTOM;

    // We need to compute the inverse of K to back-project 2D to 3D
    _invK = MatrixUtils.inverse(_k);
  }

  /*
   * Given (x,y) pixel coordinates (e.g. obtained from tag detection)
   * Returns a (X,Y) coordinate in drone space.
   */ 
  public Point2D p2m(double x, double y, double altitude) {
    // From the SDK Documentation:
    // X and Y coordinates of detected tag or oriented roundel #i inside the picture,
    // with (0; 0) being the top-left corner, and (1000; 1000) the right-bottom corner regardless
    // the picture resolution or the source camera.
    //
    // But our camera intrinsic is built for 640 x 360 pixel grid, so we must do some mapping.
    double xratio = 640 / 1000.0;
    double yratio = 360 / 1000.0;

    // Perform a simple back projection, we assume the drone is flat (no roll/pitch)
    // for the moment. We ignore the drone translation and yaw since we want X,Y in the
    // drone coordinate system.
    RealMatrix pcm = MatrixUtils.createColumnRealMatrix(new double[] 
        {x * xratio, y * yratio, 1});
    RealMatrix bp = _invK.multiply(pcm).scalarMultiply(altitude);
    RealVector pcv = bp.getColumnVector(0);
    Point2D.Double coords = new Point2D.Double(pcv.getEntry(0),pcv.getEntry(1));

    // X,Y are expressed in meters, in the drone coordinate system.
    // Which is:
    //          <--- front-facing camera
    //        |
    //       / \------- X
    //       \_/
    //        |
    //        |
    //        Y
    
    return coords;
  }  
}
