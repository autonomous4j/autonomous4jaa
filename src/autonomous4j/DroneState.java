/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package autonomous4j;

/**
 *
 * @author james_000
 */
public class DroneState {
  private double _x = 0.0;
  private double _y = 0.0;
  private double _z = 0.0;
  private double _pitch = 0.0;
  private double _roll = 0.0;
  private double _yaw = 0.0;
  private double _vx = 0.0;
  private double _vy = 0.0;
  private double _vz = 0.0;
  
  private boolean _updated = false;
  
  public double getX() {
    return _x;
  }

  public double getY() {
    return _y;
  }

  public double getZ() {
    return _z;
  }

  public double getPitch() {
    return _pitch;
  }

  public double getRoll() {
    return _roll;
  }

  public double getYaw() {
    return _yaw;
  }

  public double getVx() {
    return _vx;
  }

  public double getVy() {
    return _vy;
  }

  public double getVz() {
    return _vz;
  }

  public void setX(double x) {
    _x = x;
    _updated = true;
  }
  
  public void setY(double y) {
    _y = y;
    _updated = true;
  }
  
  public void setZ(double z) {
    _z = z;
    _updated = true;
  }
  
  public void setPitch(double pitch) {
    _pitch = pitch;
    _updated = true;
  }
  
  public void setRoll(double roll) {
    _roll = roll;
    _updated = true;
  }
  
  public void setYaw(double yaw) {
    _yaw = yaw;
    _updated = true;
  }
  
  public void setVx(double vx) {
    _vx = vx;
    _updated = true;
  }
  
  public void setVy(double vy) {
    _vy = vy;
    _updated = true;
  }
  
  public void setVz(double vz) {
    _vz = vz;
    _updated = true;
  }
  
  /**
   * Indicates whether this drone state has ever been updated, presumably with
   * navigation data from the drone
   * @return 
   */
  public boolean isUpdated() {
    return _updated;
  }
  
  @Override
  public String toString() {
    return "_x: " + _x + ", _y: " + _y +  ", _z: " + _z +  
    ", _pitch: " + _pitch + ", _roll: " + _roll +  ", _yaw: " + _yaw +  
    ", _vx: " + _vx + ", _vy: " + _vy + ", _vz: " + _vz + ", _updated: " + _updated;
  }
}
