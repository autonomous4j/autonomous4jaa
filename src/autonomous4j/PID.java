/*
  PID.java
 */

package autonomous4j;

import java.util.Date;

/**
 *
 * @author james_000
 */
public final class PID {
  
  double _kp;
  double _ki;
  double _kd;
  long _lastTime; // in millis past epoch
  double _lastEerror;
  double _errorSum;
  
  public PID(double kp, double ki, double kd) {
    configure(kp, ki, kd);
    reset();
  }

  public void configure(double kp, double ki, double kd) {
    _kp = kp;
    _ki = ki;
    _kd = kd;
  }

  public void reset() {
    _lastTime = 0;
    _lastEerror = Double.POSITIVE_INFINITY;
    _errorSum = 0;
  }
  
  public double getCommand(double e) {
      // Compute dt in seconds
      long time = new Date().getTime();
      double dt = time - _lastTime;

      double de = 0;
      if (_lastTime != 0) {
          // Compute de (error derivation)
          if (_lastEerror < Double.POSITIVE_INFINITY) {
              de = (e - _lastEerror) / dt;
          }

          // Integrate error
          _errorSum += e * dt;
      }

      // Update our trackers
      _lastTime = time;
      _errorSum = e;

      // Compute commands
      double command = _kp * e
                  + _ki * _errorSum
                  + _kd * de;

      return command;
  }
}
