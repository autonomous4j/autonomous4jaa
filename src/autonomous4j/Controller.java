package autonomous4j;

import com.dronecontrol.droneapi.DroneController;
import com.dronecontrol.droneapi.data.NavData;
import com.dronecontrol.droneapi.data.VisionTagData;
import com.dronecontrol.droneapi.listeners.ErrorListener;
import com.dronecontrol.droneapi.listeners.NavDataListener;
import com.dronecontrol.droneapi.listeners.ReadyStateChangeListener;
import com.dronecontrol.droneapi.listeners.VideoDataListener;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author James Weaver & Mark Heckler
 */
public class Controller {

    private static final double EPS_LIN = 0.1; // We are ok with 10 cm horizontal precision
    private static final double EPS_ALT = 0.1; // We are ok with 10 cm altitude precision
    private static final double EPS_ANG = 0.1; // We are ok with 0.1 rad precision (5 deg)
    private static final double STABLE_DELAY = 200; // Time in ms to wait before declaring the drone on target
    private static final double UNBOUND_GOAL = 2000; // MAH: Adequate time to conclude an "unbound goal" has been reached

    // A ardrone client to pilot the drone
    DroneController _client;

    // The position of a roundel tag to detect
    private Pose2D _roundelPos;

    // Configure the four PID required to control the drone
    private PID _pidX;
    private PID _pidY;
    private PID _pidZ;
    private PID _pidYaw;

    // kalman filter is used for the drone state estimation
    private EKF _ekf;

    // Used to process images and backproject them
    private Camera _camera;

    // Control will only work if enabled
    boolean _autoPilotEnabled = false;

    // Ensure that we don't enter the processing loop twice
    boolean _autoPilotBusy = false;

    // The current target goal and an optional callback to trigger
    // when goal is reached
    private AutoPilotGoal _autoPilotGoal = null;

    // The last known pose //TODO JLW: Remove if not needed
    //private Pose3D _lastKnownPose3D = null;
    // The last time we have reached the goal (all control commands = 0)
    // Time is millis past epoch
    private long _timeGoalLastReached = 0;

    // NavDataManager instance for listening to navigation data TODO JLW: Keep?
    //private NavDataManager _navDataManager = null;
    // Latest navdata info
    private VisionTagData _latestVisionTag;
    private DroneState _latestDroneState = new DroneState();

    // A reference to the queue containing the mission steps
    private ConcurrentLinkedQueue<MissionStep> _missionQueueRef;

    public Controller(DroneController client) {
        _client = client;
        _roundelPos = new Pose2D(0.0, 0.0, 0.0);
        _pidX = new PID(0.5, 0, 0.35);
        _pidY = new PID(0.5, 0, 0.35);
        _pidZ = new PID(0.8, 0, 0.35);
        _pidYaw = new PID(1.0, 0, 0.30);
        _ekf = new EKF(new Pose2D(0.0, 0.0, 0.0));
        _camera = new Camera();
    }

    void setMissionQueueRef(ConcurrentLinkedQueue<MissionStep> missionQueueRef) {
        _missionQueueRef = missionQueueRef;
    }

    /**
     * Enable auto-pilot. The controller will attempt to bring the drone (and
     * maintain it) to the goal.
     */
    public void enableAutoPilot() {
        _pidX.reset();
        _pidY.reset();
        _pidZ.reset();
        _pidYaw.reset();
        _autoPilotEnabled = true;
    }

    /**
     * Disable auto-pilot. The controller will stop all actions
     */
    public void disableAutoPilot() {
        _autoPilotEnabled = false;
        // TODO JLW: Provide more gentle approach (e.g. call method with landing sequence??)
        // MAH _client.stop();
    }

    /**
     * Set the auto-pilot target goal
     */
    public void setAutoPilotGoal(AutoPilotGoal autoPilotGoal) {
        _autoPilotGoal = autoPilotGoal;
    }

    /**
     * Get the auto-pilot target goal
     */
    public AutoPilotGoal getAutoPilotGoal() {
        return _autoPilotGoal;
    }

    /**
     * Return the drone state (x,y,z,yaw) as estimated by the Kalman Filter.
     */
    public DroneState getLatestDroneState() {
        return _latestDroneState;
    }

    /**
     * Sets the goal to the current state and attempt to hover on top.
     */
    public void hover() {
        Pose3D pose3D = new Pose3D(
                _latestDroneState.getX(),
                _latestDroneState.getY(),
                _latestDroneState.getZ(),
                _latestDroneState.getYaw());
        updateGoal(pose3D);
    }

    /**
     * Sets a new goal and enable the controller. When the goal is reached, the
     * callback is called with the current state.
     *
     * x,y,z in meters yaw in degrees
     */
    public void moveTo(Pose3D pose) {
        System.out.println("In Controller#moveTo(), pose arg: " + pose);
        // Why "reset" yaw? It's provided in Pose3D parameter.
        // It also prevents any spin movements...
        pose.setYaw(Math.toRadians(pose.getYaw()));
        updateGoal(pose);
    }

    /**
     * Move forward (direction faced by the front camera) by the given distance
     * (in meters).
     */
    /*
     Controller.prototype.forward = function(distance, callback) {
     // Our starting position
     var state = this.state();

     // Remap our target position in the world coordinates
     var gx = state.x + Math.cos(state.yaw) * distance;
     var gy = state.y + Math.sin(state.yaw) * distance;

     // Assign the new goal
     this._go({x: gx, y: gy, z: state.z, yaw: state.yaw}, callback);
     }
     */
    /**
     * Reset the kalman filter to its base state (default is x:0, y:0, yaw:0).
     *
     * This is especially useful to set mark the drone position as the starting
     * position after takeoff. We must disable, to ensure that the zeroing does
     * not trigger a sudden move of the drone.
     *
     * //TODO JLW: Need to finish design
     */
    public void zero() {
        disableAutoPilot();
        _ekf.reset();
    }

    public void updateGoal(Pose3D pose) {
        // Since we are going to modify goal settings, we
        // disable the controller, just in case.
        disableAutoPilot();

        // Normalize the yaw, to make sure we don't spin 360deg for
        // nothing :-)
        double yaw = pose.getYaw();
        pose.setYaw(Math.atan2(Math.sin(yaw), Math.cos(yaw)));

        // Make sure we don't attempt to go too low (1/2m minimum altitude)
        pose.setZ(Math.max(pose.getZ(), 0.5));

        // Update our goal
        _autoPilotGoal = new AutoPilotGoal(pose, false);

        // Keep track of the callback to trigger when we reach the goal
        //this._callback = callback;
        // (Re)-Enable the controller
        enableAutoPilot();
    }

    public void addNavDataListeners() {
        System.out.println("--------------------------------In addNavDataListeners()------------------------------");

        _client.addErrorListener(new ErrorListener() {
            @Override
            public void onError(Throwable thrwbl) {
                thrwbl.printStackTrace();
            }
        });

        _client.addNavDataListener(new NavDataListener() {
            @Override
            public void onNavData(NavData nd) {
                if (nd.getVisionData().getTags().isEmpty()) {
                    _latestVisionTag = null;
                } else {
                    _latestVisionTag = nd.getVisionData().getTags().get(0);
                    System.out.println("NavData received: " + nd.toString());
                    System.out.println("NavData VisionData TS --> " + nd.getVisionData().toString());
                    System.out.println("NavData VisionData Tags TS --> " + nd.getVisionData().getTags().toString());
                }

                _latestDroneState.setPitch(nd.getPitch());
                _latestDroneState.setRoll(nd.getRoll());
                _latestDroneState.setYaw(nd.getYaw());
                System.out.println("Pitch: " + nd.getPitch() + "  Roll: "
                        + nd.getRoll() + "  Yaw: " + nd.getYaw()
                        + "  Altitude: " + nd.getAltitude() / 1000.0);
                // processNavData was here; testing, 1, 2, 3...  :)

                _latestDroneState.setZ(nd.getAltitude() / 1000.0); // Store in meters

                _latestDroneState.setVx(nd.getSpeedX());  // Store in meters/sec
                _latestDroneState.setVy(nd.getSpeedY());  // Store in meters/sec
                _latestDroneState.setVz(nd.getSpeedZ());  // Store in meters/sec

                // attitudeUpdated seems to be called often, so use this to trigger 
                // calling processNavData()
                processNavData();
            }
        });

//      MAH Commented out for now, pending navigation polishing. 20140829
//        _client.addReadyStateChangeListener(new ReadyStateChangeListener() {
//            @Override
//            public void onReadyStateChange(ReadyStateChangeListener.ReadyState rs) {
//                System.out.println("Ready State Change: " + rs.toString());
//            }
//        });
//        
//        _client.addVideoDataListener(new VideoDataListener() {
//            @Override
//            public void onVideoData(BufferedImage bi) {
//                System.out.println("Receiving Video Data...");
//            }
//        });
    }

    private void processNavData() {
        // Ensure that we don't enter the processing loop twice
        if (_autoPilotBusy) {
            System.out.println("PND: autopilot busy");
            return;
        }
        _autoPilotBusy = true;

        // EKF prediction step
        _ekf.predict(_latestDroneState);

        // If a tag is detected by the bottom camera, we attempt a correction step
        // This requires prior configuration of the client to detect the oriented
        // roundel and to enable the vision detect in navdata.
        if (_latestVisionTag != null) {
            System.out.println("PND: processing VisionTag");
            // Fetch detected tag position, size and orientation
            int xc = _latestVisionTag.getX();
            int yc = _latestVisionTag.getY();
            int wc = _latestVisionTag.getWidth();
            int hc = _latestVisionTag.getHeight();
            double yaw = _latestVisionTag.getOrientationAngle();
            double dist = _latestVisionTag.getDistance() / 100.0; // Need meters

            // Compute measure tag position (relative to drone) by
            // back-projecting the pixel position p(x,y) to the drone
            // coordinate system P(X,Y).
            // TODO: Should we use dist or the measure altitude ?
            Point2D camPoint = _camera.p2m(xc + wc / 2, yc + hc / 2, dist);

            // We convert this to the controller coordinate system
            Pose2D measured = new Pose2D(-1 * camPoint.getY(), camPoint.getX(), 0.0);

            // Rotation is provided by the drone, we convert to radians
            measured.setYaw(Math.toRadians(yaw));

            // Execute the EKS correction step
            _ekf.correct(measured, _roundelPos);
        }

        // Keep a local copy of the 2D pose
        // Note: Altitude and x/y velocity are updated as events from drone are received 
        Pose2D pose = _ekf.getPose2D();
        _latestDroneState.setX(pose.getX());
        _latestDroneState.setY(pose.getY());

        control();
        _autoPilotBusy = false;
    }

    private void control() {
        // Do not control if not enabled
        if (!_autoPilotEnabled) {
            System.out.println("Control: autopilot not enabled");
            return;
        }

        // Do not control if no goal defined or no known state
        if (_autoPilotGoal == null || !_latestDroneState.isUpdated()) {
            if (_autoPilotGoal == null) {
                System.out.println("Control: null autopilot goal");
            } else {
                System.out.println("Control: drone state not updated");
            }
            // MAH: Extending the concept of a goal to include so-called "unbound goals",
            // e.g. takeoff & landing. These "goals" don't have geolocational points that
            // allow us to say they have been reached...so we time them. :)
            //
            // MAH: May want to just "fall through" anyway, since we allow the user/dev
            // to specify a doFor/hold time. Revisit.
            if (_timeGoalLastReached != 0) {
                if ((new Date().getTime() - _timeGoalLastReached) > UNBOUND_GOAL) {
                    //_timeGoalLastReached = 0;
                    _timeGoalLastReached = new Date().getTime();
                    System.out.println("Control: reset time of last goal reached (post-unbound)");

                    try {
                        System.out.println("Control: loading next mission step (post-unbound)");
                        MissionStep missionStep = _missionQueueRef.remove();
                        missionStep.perform();
                    } catch (NoSuchElementException nsee) {
                        // No more mission steps so stop drone
                        System.out.println("Control: Mission steps exhausted, stopping client.");
                        _client.stop();
                    }
                }
            } else {
                _timeGoalLastReached = new Date().getTime();
            }
            return;
        }

        // Compute error between current state and goal
        double ex = _autoPilotGoal.getX() - _latestDroneState.getX();
        double ey = _autoPilotGoal.getY() - _latestDroneState.getY();
        double ez = _autoPilotGoal.getY() - _latestDroneState.getZ();
        double eyaw = _autoPilotGoal.getYaw() - _latestDroneState.getYaw();
        // MAH
        System.out.println("\t---------- Goal Delta ----------\n"
                + "\tX delta  : " + ex + "\n"
                + "\tY delta  : " + ey + "\n"
                + "\tZ delta  : " + ez + "\n"
                + "\tyaw delta: " + eyaw);

        // Normalize eyaw within [-180, 180]
        while (eyaw < -Math.PI) {
            eyaw += (2 * Math.PI);
        }
        while (eyaw > Math.PI) {
            eyaw -= (2 * Math.PI);
        }

        // Check if we are within the target area
        if ((Math.abs(ex) < EPS_LIN) && (Math.abs(ey) < EPS_LIN)
                && (Math.abs(ez) < EPS_ALT) && (Math.abs(eyaw) < EPS_ANG)) {
            System.out.println("Control: within target area");

            // Have we been here before ?
            if (!_autoPilotGoal.isReached() && _timeGoalLastReached != 0) {
                // And for long enough ?
                if ((new Date().getTime() - _timeGoalLastReached) > STABLE_DELAY) {
                    // Mark the goal as reached
                    _autoPilotGoal.setReached(true);
                    System.out.println("Control: autopilot goal reached!");

                    // Execute the next mission step
                    // TODO JLW: Examine threading ramifications.  Also, what is best way
                    // to execute next step when there is no goal associated with a step
                    // (such as takeoff or landing)?
                    try {
                        System.out.println("Control: performing mission step");
                        MissionStep missionStep = _missionQueueRef.remove();
                        missionStep.perform();
                    } catch (NoSuchElementException nsee) {
                        // No more mission steps so stop drone
                        System.out.println("No more mission steps to process; stopping client.");
                        _client.stop();
                    }

                    /*
                     if (this._callback != null) {
                     setTimeout(this._callback, 10);
                     this._callback = null;
                     }
                     */
                    // Emit a state reached
                    System.out.println("******** Autonomous4j info: ********"
                            + "Goal reached, _latestDroneState: " + _latestDroneState
                            + "\n****** End Autonomous4j info ******\n");
                }
            } else {
                _timeGoalLastReached = new Date().getTime();
                System.out.println("Control: reset time of last goal reached");
            }
        } else {
            System.out.println("Control: just left the goal");
            // If we just left the goal, we notify
            if (_timeGoalLastReached != 0) {
                // Reset last ok since we are in motion
                _timeGoalLastReached = 0;
                _autoPilotGoal.setReached(true);

//                System.out.println("******** Autonomous4j info: ********"
//                        + "\nGoal exited, _latestDroneState: " + _latestDroneState
//                        + "\n****** End Autonomous4j info ******\n");
                System.out.println("Goal exited.");
            }
        }

        // Get Raw command from PID
        double ux = _pidX.getCommand(ex);
        double uy = _pidY.getCommand(ey);
        double uz = _pidZ.getCommand(ez);
        double uyaw = _pidYaw.getCommand(eyaw);

        // Ceil commands and map them to drone orientation
        double yaw = _latestDroneState.getYaw();
        double cx = within(Math.cos(yaw) * ux + Math.sin(yaw) * uy, -1, 1);
        double cy = within(-Math.sin(yaw) * ux + Math.cos(yaw) * uy, -1, 1);
        double cz = within(uz, -1, 1);
        double cyaw = within(uyaw, -1, 1);

//      Commenting for now to thin the data feed!
//        // Emit the control data for auditing
//        System.out.println("******** Autonomous4j control data: ********"
//                + "\n_latestDroneState: " + _latestDroneState
//                + "\n_autoPilotGoal: " + _autoPilotGoal
//                + "\nerror: {ex: " + ex + ", ey: " + ey + ", ez: " + ez + ", eyaw: " + eyaw + "}"
//                + "\ncontrol: {ux: " + ux + ", uy: " + uy + ", uz: " + uz + ", uyaw: " + uyaw + "}"
//                + "\n_timeGoalLastReached:" + _timeGoalLastReached
//                + "\n_latestVisionTag: " + (_latestVisionTag != null)
//                + "\n****** End Autonomous4j controlData ******\n");

        // Send commands to drone.  Note that CommandManager expects percentages from 1 to 100
        //CommandManager cmdMgr = _client.getCommandManager();
        if (Math.abs(cx) > 0.01) {
            //cmdMgr.forward((int) (cx * 100));
            // MAH: forward or backward
            System.out.println("---------------> Moving " + (cx > 0 ? "for" : "back") + "ward: " + cx);
            _client.move(0f, (float) (cx * 100), 0f, 0f);
        }

        if (Math.abs(cy) > 0.01) {
            //cmdMgr.goRight((int) (cy * 100));
            // MAH: left or right
            System.out.println("---------------> Moving " + (cy > 0 ? "right" : "left") + ": " + cy);
            _client.move((float) (cy * 100), 0f, 0f, 0f);
        }

        if (Math.abs(cz) > 0.01) {
            //cmdMgr.up((int) (cz * 100));
            // MAH: up or down
            System.out.println("---------------> Moving " + (cz > 0 ? "up" : "down") + ": " + cz);
            _client.move(0f, 0f, (float) (cz * 100), 0f);
        }

        if (Math.abs(cyaw) > 0.01) {
            //cmdMgr.spinRight((int) (cyaw * 100));
            // MAH: spin left or right
            System.out.println("---------------> You spin me " 
                    + (cyaw > 0 ? "right" : "left") + " round: " + cyaw);
            _client.move(0f, 0f, 0f, (float) (cyaw * 100));
        }
    }

    private double within(double x, double min, double max) {
        if (x < min) {
            return min;
        } else if (x > max) {
            return max;
        } else {
            return x;
        }
    }

}
