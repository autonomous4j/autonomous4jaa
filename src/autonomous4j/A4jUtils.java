package autonomous4j;

import com.dronecontrol.droneapi.DroneController;
import com.dronecontrol.droneapi.ParrotDroneController;
import com.dronecontrol.droneapi.data.Config;
import com.dronecontrol.droneapi.listeners.ErrorListener;

/**
 *
 * @author Jim Weaver & Mark Heckler
 */
public class A4jUtils {
    private static DroneController createClient() {
        DroneController drone = null;
        try {
            drone = ParrotDroneController.build();
            drone.addErrorListener(new ErrorListener() {
                @Override
                public void onError(Throwable thrwbl) {
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    System.out.println("Throwable error: " + thrwbl.getLocalizedMessage());
                    thrwbl.printStackTrace();
                }

            });
//			drone.addExceptionListener(new IExceptionListener() {
//				public void exeptionOccurred(ARDroneException exc) {
//					exc.printStackTrace();
//				}
//			});
        } catch (Exception exc) {
            exc.printStackTrace();
        }
        /*
         //TODO JLW: Put back in some form?
         finally {
         if (drone != null)
         drone.stop();

         System.exit(0);
         }
         */
        return drone;
    }

    public static Mission createMission() {
        DroneController drone = createClient();
        Controller controller = new Controller(drone);
        Mission mission = new Mission(drone, controller);

        drone.start(new Config("Autonomous4j Test w/Parroteer Lib", "A4jP Profile", 0));
        controller.addNavDataListeners();

        return mission;
    }
}
