/*
 * EKF.java
 */

package autonomous4j;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

/**
 *
 * @author james_000
 */
public final class EKF {
  static final double DELTA_T = 1 / 15; // In demo mode, 15 navdata per second
  
  private double _deltaT;
  private Pose2D _pose2D;
  private RealMatrix _sigma;
  private RealMatrix _q;
  private RealMatrix _r;
  private double _lastYaw;
  
  private Pose2D _s;
  private Pose2D _m;
  private Pose2D _z;
  private Pose2D _e;
  
  public EKF(Pose2D state) {
    _deltaT = DELTA_T;
    reset();
  }
  
  public Pose2D getPose2D() {
    return _pose2D;
  }

  public RealMatrix getConfidence() {
    return _sigma;
  }

  public void reset() {
    if (_pose2D == null) {
      _pose2D = new Pose2D(0.0, 0.0, 0.0);
    }
    
    _sigma = MatrixUtils.createRealIdentityMatrix(3);
    
    double[] qDiagElements = {0.0003, 0.0003, 0.0001};
    _q = MatrixUtils.createRealDiagonalMatrix(qDiagElements);
    
    double[] rDiagElements = {0.3, 0.3, 0.3};
    _r = MatrixUtils.createRealDiagonalMatrix(rDiagElements);
    
    _lastYaw = 0.0;    
  }
  
  public void predict(DroneState droneState) {
    double pitch = Math.toRadians(droneState.getPitch()); //TODO JLW: Not used?
    double roll = Math.toRadians(droneState.getRoll()); //TODO JLW: Not used?
    double yaw = normAngle(droneState.getYaw());
    double vx = droneState.getVx() / 1000.0;
    double vy = droneState.getVy() / 1000.0;
    double dt = _deltaT;
    
    // We are not interested by the absolute yaw, but the yaw motion,
    // so we need at least a prior value to get started.
    if (_lastYaw == 0.0) {
      _lastYaw = yaw;
      return;
    }
    
    // Compute the odometry by integrating the motion over delta_t
    Pose2D odo = new Pose2D(vx * dt, vy * dt, yaw - _lastYaw);
    _lastYaw  = yaw;
    
    // Update the state estimate
    Pose2D state = _pose2D;
    state.setX(state.getX() + odo.getX() * Math.cos(state.getYaw()) - odo.getY() * Math.sin(state.getYaw()));
    state.setY(state.getY() + odo.getX() * Math.sin(state.getYaw()) - odo.getY() * Math.cos(state.getYaw()));
    state.setYaw(state.getYaw() + odo.getYaw());

    // Normalize the yaw value
    state.setYaw(Math.atan2(Math.sin(state.getYaw()),Math.cos(state.getYaw())));

    // Compute the G term (due to the Taylor approximation to linearize the function).
    double[][] gMatData = {
      {1, 0, -1 * Math.sin(state.getYaw()) * odo.getX() - Math.cos(state.getYaw()) * odo.getY()},
      {0, 1,  Math.cos(state.getYaw()) * odo.getX() - Math.sin(state.getYaw()) * odo.getY()},
      {0, 0, 1}
    };
    RealMatrix g = MatrixUtils.createRealMatrix(gMatData);

    // Compute the new sigma
    _sigma = g.multiply(_sigma).multiply(g.transpose()).add(this._q);
  }

 /*
  * measure.x:   x-position of marker in drone's xy-coordinate system (independent of roll, pitch)
  * measure.y:   y-position of marker in drone's xy-coordinate system (independent of roll, pitch)
  * measure.yaw: yaw rotation of marker, in drone's xy-coordinate system (independent of roll, pitch)
  *
  * pose.x:   x-position of marker in world-coordinate system
  * pose.y:   y-position of marker in world-coordinate system
  * pose.yaw: yaw-rotation of marker in world-coordinate system
  */
  public void correct(Pose2D measure, Pose2D pose) {
    // Compute expected measurement given our current state and the marker pose
    Pose2D state = _pose2D;
    double psi = state.getYaw();
    _s = new Pose2D(state.getX(), state.getY(), state.getYaw());

    // Normalized the measure yaw
    measure.setYaw(normAngle(measure.getYaw()));
    _m = new Pose2D(measure.getX(), measure.getY(), measure.getYaw());

    double z1 = Math.cos(psi) * (pose.getX() - state.getX()) + Math.sin(psi) * (pose.getY() - state.getY());
    double z2 = -1 * Math.sin(psi) * (pose.getX() - state.getX()) + Math.cos(psi) * (pose.getY() - state.getY());
    double z3 = pose.getYaw() - psi;
    _z = new Pose2D(z1, z2, z3);

    // Compute the error
    double e1 = measure.getX() - z1;
    double e2 = measure.getY() - z2;
    double e3 = measure.getYaw() - z3;
    _e = new Pose2D(e1, e2, e3);

    // Compute the H term
    double[][] hMatData = {
      {-Math.cos(psi), -Math.sin(psi), Math.sin(psi) * (state.getX() - pose.getX()) - Math.cos(psi) * (state.getY() - pose.getY())},
      {Math.sin(psi), -Math.cos(psi), Math.cos(psi) * (state.getX() - pose.getX()) + Math.sin(psi) * (state.getY() - pose.getY())},
      {0, 0, -1}
    };
    RealMatrix h = MatrixUtils.createRealMatrix(hMatData);

    // Compute the Kalman Gain
    RealMatrix ht = h.transpose();
    RealMatrix k = MatrixUtils.inverse(_sigma.multiply(ht).multiply(h.multiply(_sigma).multiply(ht).add(_r)));

    // Correct the pose estimate
    RealMatrix err = MatrixUtils.createColumnRealMatrix(new double[] 
        {e1, e2, e3});
    RealMatrix c = k.multiply(err);
    RealVector ccv = c.getColumnVector(0);
    state.setX(ccv.getEntry(0));
    state.setY(ccv.getEntry(1));

//  TODO - This does not work, need more investigation.
//  In the meanwhile, we don't correct yaw based on observation.
//  state.yaw = state.yaw + c.e(3);

    _sigma = MatrixUtils.createRealIdentityMatrix(3).subtract(k.multiply(h)).multiply(_sigma);
};
  
  
  private double normAngle(double rad) {
    while (rad >  Math.PI) { rad -= 2 * Math.PI;}
    while (rad < -Math.PI) { rad += 2 * Math.PI;}
    return rad;
  }
}
