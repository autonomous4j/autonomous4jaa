//TODO JLW: Add mission logging, and land/stop drone if unrecoverable error
package autonomous4j;

import com.dronecontrol.droneapi.DroneController;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jim Weaver & Mark Heckler
 */
public class Mission {
    private DroneController _client;
    private Controller _controller;
    private ConcurrentLinkedQueue<MissionStep> _missionQueue;

    public Mission(DroneController client, Controller controller) {
        _client = client;
        _controller = controller;
        _missionQueue = new ConcurrentLinkedQueue();
        _controller.setMissionQueueRef(_missionQueue);
    }

    public DroneController getClient() {
        return _client;
    }

    public Controller getController() {
        return _controller;
    }

    public ConcurrentLinkedQueue<MissionStep> getMissionQueue() {
        return _missionQueue;
    }

    public void executeMission() {
        //MAH _client.start();

        //MAH _controller.addNavDataListeners();
        _controller.enableAutoPilot();
        //CommandManager cmdMgr = _client.getCommandManager();
        //cmdMgr.setDetectionType(CadType.ORIENTED_COCARDE_BW);
        //cmdMgr.setFlyingMode(FlyingMode.FREE_FLIGHT);

        // Execute the first mission step
        MissionStep missionStep = _missionQueue.remove();
        System.out.println("----> missionStep: " + missionStep);
        missionStep.perform(); //TODO JLW: Verify that this works correctly

//        _client.start();

    }

    public Mission takeoff() {
        //_missionQueue.add(() -> _client.takeOff());
        _missionQueue.add(() -> {
            System.out.println("------> In lambda requesting takeOff");
            _client.takeOff();
        });
        return this;
    }

    public Mission forward() {
        _missionQueue.add(() -> {
            System.out.println("------> In lambda requesting Mission#Forward");
            //return move(0f, 1f, 0f, 0f);
            moveTo(new Pose3D(1d, 0d, 0d, 0d));
        });
        return this;
    }

    public Mission backward() {
        _missionQueue.add(() -> {
            System.out.println("------> In lambda requesting Mission#Backward");
            //return move(0f, 1f, 0f, 0f);
            moveTo(new Pose3D(-1d, 0d, 0d, 0d));
        });
        return this;
    }

    public Mission goRight() {
        _missionQueue.add(() -> {
            System.out.println("------> In lambda requesting Mission#goRight");
            //return move(perc2float(speed), 0f, 0f, 0f);
            moveTo(new Pose3D(0d, 1d, 0d, 0d));
        });
        return this;
    }

    public Mission goLeft() {
        _missionQueue.add(() -> {
            System.out.println("------> In lambda requesting Mission#goLeft");
            //return move(-perc2float(speed), 0f, 0f, 0f);
            moveTo(new Pose3D(0d, -1d, 0d, 0d));
        });
        return this;
    }

    public Mission up() {
        _missionQueue.add(() -> {
            System.out.println("------> In lambda requesting Mission#up");
            //return move(0f, 0f, perc2float(speed), 0f);
            moveTo(new Pose3D(0d, 0d, 1d, 0d));
        });
        return this;
    }

    public Mission down() {
        _missionQueue.add(() -> {
            System.out.println("------> In lambda requesting Mission#down");
            //return move(0f, 0f, -perc2float(speed), 0f);
            moveTo(new Pose3D(0d, 0d, -1d, 0d));
        });
        return this;
    }

//    public Mission hover(long ms) {
//        _missionQueue.add(() -> {
//            System.out.println("In lambda hovering for " + ms + " milliseconds...");
//            //_client.hover();
//            // MAH: hover
//            _client.move(0f, 0f, 0f, 0f);                    
//            hold(ms);
//        });
//        return this;
//    }

    public Mission hover() {
        _missionQueue.add(() -> {
            System.out.println("------> In lambda hovering...");
            //_client.hover();
            // MAH: hover
            _client.move(0f, 0f, 0f, 0f);                    
        });
        return this;        
    }
    
    public Mission doFor(long ms) {
        return hold(ms);
    }
    
    public Mission hold(long ms) {
        _missionQueue.add(() -> {
            try {
                System.out.println("------> Hold for " + ms + " milliseconds...");
                Thread.sleep(ms);
            } catch (InterruptedException e) {
                // MAH revisit
                e.printStackTrace();
                Logger.getLogger(Mission.class.getName()).log(Level.SEVERE, null, e);
            }
        });
        return this;
    }
    
    public Mission land() {
        //_missionQueue.add(() -> _client.landing());
        _missionQueue.add(() -> {
            System.out.println("------> In lambda requesting landing");
            _client.land();
        });
        return this;
    }

    public Mission moveTo(Pose3D pose) {
        //_missionQueue.add(() -> _controller.moveTo(pose));
        _missionQueue.add(() -> {
            System.out.println("------> In lambda requesting moveTo");
            _controller.moveTo(pose);
        });
        return this;
    }

}
