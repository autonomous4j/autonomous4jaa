/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package autonomous4j;

/**
 *
 * @author james_000
 */
public class Pose3D extends Pose2D {
  private double _z = 0.0;
  
  public Pose3D(double x, double y, double z, double yaw) {
    super(x, y, yaw);
    _z = z;
  }
  
  public double getZ() {
    return _z;
  }

  public void setZ(double z) {
    _z = z;
  }

  @Override
  public String toString() {
    return super.toString() + "\n_z: " + _z;
  }
  
}
