/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package autonomous4j;

/**
 *
 * @author james_000
 */
public class Pose2D {
  private double _x = 0.0;
  private double _y = 0.0;
  private double _yaw = 0.0;
  
  public Pose2D(double x, double y, double yaw) {
    _x = x;
    _y = y;
    _yaw = yaw;
  }
  
  public double getX() {
    return _x;
  }
  
  public double getY() {
    return _y;
  }
  
  public double getYaw() {
    return _yaw;
  }

  public void setX(double x) {
    _x = x;
  }
  
  public void setY(double y) {
    _y = y;
  }
  
  public void setYaw(double yaw) {
    _yaw = yaw;
  }

  @Override
  public String toString() {
    return "_x: " + _x + ", _y: " + _y +   
    ", _yaw: " + _yaw;
  }
  
}
