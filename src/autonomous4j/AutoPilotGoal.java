/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package autonomous4j;

/**
 *
 * @author james_000
 */
public class AutoPilotGoal extends Pose3D{
  private boolean _reached = false;
  
  /*
  public AutoPilotGoal(double x, double y, double z, double yaw, boolean reached) {
    super(x, y, z, yaw);
    _reached = reached;
  }
  */

  public AutoPilotGoal(Pose3D state3D, boolean reached) {
    super(state3D.getX(), state3D.getY(), state3D.getZ(), state3D.getYaw());
    _reached = reached;
  }
  
  public AutoPilotGoal(Pose3D state3D) {
    super(state3D.getX(), state3D.getY(), state3D.getZ(), state3D.getYaw());
    _reached = false;
  }
  
  public boolean isReached() {
    return _reached;
  }
  
  public void setReached(boolean reached) {
    _reached = reached;
  }
  
  @Override
  public String toString() {
    return super.toString() + "\n_reached: " + _reached;
  }  
}
