/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package autonomous4j;

/**
 *
 * @author james_000
 */
@FunctionalInterface
public interface MissionStep {
  public abstract void perform();
  
}
